const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

// create a schema
const userSchema = new Schema({


    method: {
        type: String,
        enum: ['local', 'google', 'facebook'],
        required: true
    },
    local: {
        email: {
            type: String,
            lowercase: true
        },
        password:  {
            type: String
        }
    },
    google: {
        id: {
            type: String
        },
        email: {
            type: String,
            lowercase: true
        }
    },
    facebook :{
        id: {
            type: String
        },
        email: {
            type: String,
            lowercase: true
        }
    }
});

userSchema.pre('save', async function(next) {
    try {


        if(this.method !== 'local') {
            next();
        }

        // generate a salt 
        const salt = await bcrypt.genSalt(10);
        // generate hashed password = salt + hash
        const passwordHash = await bcrypt.hash(this.local.password, salt);
        
        // re assign hashed version of password to password
        this.local.password = passwordHash;
        next();
        //console.log('salt', salt);
        //console.log('normal password', this.password);
        //console.log('hashed password', passwordHash);
    } catch (error) {
        next(error);
    }
});

userSchema.methods.isValidPassword = async function(newPassword) {
    try {
        console.log('local password',this.local.password)
        console.log('new pwd', newPassword);
        return await bcrypt.compare(newPassword, this.local.password);
    } catch(error) {
        throw new Error(error);
    }
}

// create a model
const User = mongoose.model('user', userSchema);


// export the model 
module.exports = User;